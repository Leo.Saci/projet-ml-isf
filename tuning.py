import matplotlib.pyplot as plt

plt.ion()
import numpy as np
import pandas as pd
from process_data import load_data, transform_data
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.metrics import (
    accuracy_score,
    f1_score,
    precision_score,
    recall_score,
    roc_curve,
    roc_auc_score,
    precision_recall_curve,
    confusion_matrix,
    cohen_kappa_score,
    auc,
)
import seaborn as sns

plt.rcParams.update({"font.size": 12, "text.usetex": True, "text.latex.preamble": r"\usepackage{amsfonts}"})

### MODEL
MODEL = "GradientBoosting"

### SEED
seed = 42

### LOADING AND PROCESSING DATA
data, target = load_data()
X = transform_data(data)
l = list(range(X.shape[1]))
# l.remove(2)
# X = X.iloc[:, l]
# X = X.iloc[:, [2]]
y = target

### TRAIN TEST SPLIT
size = 0.3
print()
print(f"TRAIN-TEST SPLIT : {(1 - size) * 100}% - {size * 100}%")
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=size, random_state=seed)


### THE MODELS
model2name = {
    "logistic": "Régression Logistique",
    "tree": "Arbre de décision",
    "forest": "Random Forest",
    "GradientBoosting": "Gradient Boosting",
    "MLP": "MLP Classifier",
}
model_name = model2name[MODEL]

print()
print("NOM DU MODELE : " + model2name[MODEL])
print()


if MODEL == "logistic":
    model = LogisticRegression(random_state=seed)
    scoring = "f1"

    param_grid = {"penalty": ["l2"], "C": [0.8], "random_state": [seed]}
    grid_search = GridSearchCV(model, param_grid, cv=5, scoring=scoring, error_score="raise")
    grid_search.fit(X_train, y_train)
    print("Meilleurs hyperparamètres:", grid_search.best_params_)
    print()

    model_opt = LogisticRegression(**grid_search.best_params_)


elif MODEL == "tree":
    model = DecisionTreeClassifier(random_state=seed)
    scoring = "f1"

    param_grid = {
        "criterion": ["entropy", "log_loss"],
        "max_depth": [None],
        "min_samples_split": [5, 10, 15, 20, 25],
        "min_samples_leaf": [4, 8, 10],
        "random_state": [seed],
    }

    grid_search = GridSearchCV(model, param_grid, cv=10, scoring=scoring, error_score="raise")
    grid_search.fit(X_train, y_train)
    print("Meilleurs hyperparamètres:", grid_search.best_params_)
    print()

    model_opt = DecisionTreeClassifier(**grid_search.best_params_)


elif MODEL == "forest":
    model = RandomForestClassifier(random_state=seed)
    scoring = "f1"

    param_grid = {
        "n_estimators": [10, 25],
        "criterion": ["log_loss"],
        "max_depth": [None, 5, 10],
        "min_samples_split": [5, 10],
        "min_samples_leaf": [4],
        "bootstrap": [False],
        "random_state": [seed],
    }

    grid_search = GridSearchCV(model, param_grid, cv=5, scoring=scoring, error_score="raise")
    grid_search.fit(X_train, y_train)
    print("Meilleurs hyperparamètres:", grid_search.best_params_)
    print()

    model_opt = RandomForestClassifier(**grid_search.best_params_)


elif MODEL == "GradientBoosting":
    model = GradientBoostingClassifier(random_state=seed)
    scoring = "f1"

    param_grid = {
        "learning_rate": [0.2],
        "max_depth": [4],
        "min_samples_leaf": [5],
        "min_samples_split": [5],
        "n_estimators": [300],
        "random_state": [seed],
    }

    grid_search = GridSearchCV(model, param_grid, cv=5, scoring=scoring, error_score="raise")
    grid_search.fit(X_train, y_train)
    print("Meilleurs hyperparamètres:", grid_search.best_params_)
    print()

    model_opt = GradientBoostingClassifier(**grid_search.best_params_)


elif MODEL == "MLP":
    model = MLPClassifier(max_iter=1000, random_state=seed)
    scoring = "f1"

    param_grid = {
        "hidden_layer_sizes": [(100,)],
        "activation": ["relu"],
        "alpha": [0.05, 0.1],
        "learning_rate": ["adaptive"],
        "max_iter": [1000],
        "random_state": [seed],
    }

    grid_search = GridSearchCV(model, param_grid, cv=5, scoring=scoring)  # , error_score='raise')
    grid_search.fit(X_train, y_train)
    print("Meilleurs hyperparamètres:", grid_search.best_params_)
    print()

    model_opt = MLPClassifier(**grid_search.best_params_)


# CHOOSE THE METRICS
metrics = ["accuracy", "f1", "precision", "recall", "kappa"]
ref_score = "f1"
lab2names = {
    "accuracy": "Accuracy",
    "f1": "F1-Score",
    "precision": "Precision",
    "recall": "Recall",
    "kappa": "Kappa Score",
}
plot_feat_importance = True


## COMPARAISON ENTRE LE MODELE PAR DEFAUT ET LE MODELE OPTIMISE

print("***SCORES DU MODELE NON OPTIMISE***")
print()
model.fit(X_train, y_train)
y_pred = model.predict(X_test)

# PRINT SCORE
if "accuracy" in metrics:
    accuracy = accuracy_score(y_test, y_pred)
    print("Accuracy sur le test : {:.3f}%".format(accuracy * 100))

if "precision" in metrics:
    precision = precision_score(y_test, y_pred)
    print("Précision sur le test : {:.3f}".format(precision))

if "recall" in metrics:
    precision = recall_score(y_test, y_pred)
    print("Recall sur le test : {:.3f}".format(precision))

if "f1" in metrics:
    f1 = f1_score(y_test, y_pred)
    print("F1-Score sur le test : {:.3f}".format(f1))

if "kappa" in metrics:
    kappa = cohen_kappa_score(y_test, y_pred)
    print("Kappa score sur le test : {:.3f}".format(kappa))
print()

print()
print("***SCORES DU MODELE OPTIMISE***")
print()
model_opt.fit(X_train, y_train)
y_pred_opt = model_opt.predict(X_test)

# PRINT SCORE
if "accuracy" in metrics:
    accuracy = accuracy_score(y_test, y_pred_opt)
    print("Accuracy sur le test : {:.3f}%".format(accuracy * 100))

if "precision" in metrics:
    precision = precision_score(y_test, y_pred_opt)
    print("Précision sur le test : {:.3f}".format(precision))

if "recall" in metrics:
    precision = recall_score(y_test, y_pred_opt)
    print("Recall sur le test : {:.3f}".format(precision))

if "f1" in metrics:
    f1 = f1_score(y_test, y_pred_opt)
    print("F1-Score sur le test : {:.3f}".format(f1))

if "kappa" in metrics:
    kappa = cohen_kappa_score(y_test, y_pred_opt)
    print("Kappa score sur le test : {:.3f}".format(kappa))
print()


y_scores = model.predict_proba(X_test)[:, 1]
y_scores_opt = model_opt.predict_proba(X_test)[:, 1]


### ROC CURVES
fpr, tpr, seuils = roc_curve(y_test, y_scores)
fpr_opt, tpr_opt, seuils_opt = roc_curve(y_test, y_scores_opt)

AUC = roc_auc_score(y_test, y_scores)
AUC_opt = roc_auc_score(y_test, y_scores_opt)

plt.figure(1)
plt.plot([0, 1], [0, 1], color="gray", linestyle="--", lw=1, label="No skills")
plt.plot(fpr, tpr, lw=2, label="Paramètres par défaut" + " (AUC = {:.3f})".format(AUC))
plt.plot(fpr_opt, tpr_opt, lw=2, label="Paramètres optimisés" + " (AUC = {:.3f})".format(AUC_opt))
plt.xlabel("Taux de Faux Positifs ($\\frac{FP}{FP + TN}$)")
plt.ylabel("Taux de Vrais Positifs ($\\frac{TP}{TP + FN}$)")
plt.title("Courbes ROC : " + model_name)
plt.legend()

### PRECISION - RECALL

p = sum(y_train) / len(y_train)

precision, recall, seuils = precision_recall_curve(y_test, y_scores)
precision_opt, recall_opt, seuils_opt = precision_recall_curve(y_test, y_scores_opt)

AUC = auc(recall, precision)
AUC_opt = auc(recall_opt, precision_opt)

plt.figure(2)
plt.plot(
    [0, 1], [p, p], color="gray", linestyle="--", lw=1, label="No skills ($\\frac{TP}{TP + FP + TN + FN}$)"
)
plt.plot(recall, precision, lw=2, label="Paramètres par défaut" + " (AUC = {:.4f})".format(AUC))
plt.plot(recall_opt, precision_opt, lw=2, label="Paramètres optimisés" + " (AUC = {:.4f})".format(AUC_opt))
plt.xlabel("Recall " + "= $\\frac{TP}{TP + FN}$")
plt.ylabel("Precision " + "= $\\frac{TP}{TP + FP}$")
plt.xlim([0, 1])
plt.ylim([0.57, 1])
plt.title("Precision-Recall Curve")
plt.legend()

### CONFUSION MATRICES
confusion = True

if confusion:
    m = confusion_matrix(y_test, y_pred)
    plt.figure(42)
    sns.heatmap(
        m,
        annot=True,
        fmt="d",
        cmap="Blues",
        xticklabels=["Prédit 0", "Prédit 1"],
        yticklabels=["Vrai 0", "Vrai 1"],
    )
    plt.xlabel("Prédictions")
    plt.ylabel("Vraies étiquettes")
    plt.title("Matrice de Confusion sur le test : " + model_name + " non optimisé")

    m_opt = confusion_matrix(y_test, y_pred_opt)
    plt.figure(43)
    sns.heatmap(
        m_opt,
        annot=True,
        fmt="d",
        cmap="Blues",
        xticklabels=["Prédit 0", "Prédit 1"],
        yticklabels=["Vrai 0", "Vrai 1"],
    )
    plt.xlabel("Prédictions")
    plt.ylabel("Vraies étiquettes")
    plt.title("Matrice de Confusion sur le test : " + model_name + " optimisé")
