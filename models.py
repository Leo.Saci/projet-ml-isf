import numpy as np


class Random:
    def __init__(self, random_state=42, p=0.5):
        self.name = "Stratégie Aléatoire"
        self.proba = p
        self.seed = random_state

    def fit(self, X, y):
        dim = X.shape[1]
        self.feature_importances_ = np.ones(dim) / dim
        return None

    def predict(self, X):
        np.random.seed(self.seed)
        dim = X.shape[0]
        p = self.proba
        y_pred = np.random.choice([0, 1], size=dim, p=[1 - p, p])
        return y_pred

    def get_params(self, deep=False):
        return {}

    def predict_proba(self, X):
        dim = X.shape[0]
        P = np.random.rand(dim, 2)
        return P


class Combine:
    def __init__(self, models, random_state=42):
        self.name = "Combinaison de modèles"
        self.seed = random_state
        self.models = models

    def fit(self, X, y):
        probas = np.zeros((y.shape[0], 2))
        for weight, model in self.models.items():
            model.fit(X, y)
        return None

    def predict_proba(self, X):
        dim = X.shape[0]
        probas = np.zeros((dim, 2))
        for weight, model in self.models.items():
            probas = probas + weight * model.predict_proba(X)
        probas = probas / len(self.models)
        return probas

    def predict(self, X):
        probas = self.predict_proba(X)
        y_pred = np.argmax(probas, axis=1)
        return y_pred
