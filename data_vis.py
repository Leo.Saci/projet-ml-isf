import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

plt.ion()
from scipy.stats import chi2_contingency
import seaborn as sns

plt.rcParams.update({"font.size": 12, "text.usetex": True, "text.latex.preamble": r"\usepackage{amsfonts}"})


def plot_categorielles(data, var_cat_names):
    fig, axs = plt.subplots(2, 2, figsize=(8, 6))
    plt.suptitle("Variables catégorielles")

    titles = [el.replace('_', ' ').title() for el in var_cat_names]
    name1 = var_cat_names[0]
    title1 = titles[0]
    categories_counts1 = data[name1].value_counts()
    bars1 = axs[0, 0].bar(
        categories_counts1.index, categories_counts1.values, color="green", edgecolor="black", alpha=0.4
    )
    for bar in bars1:
        yval = bar.get_height()
        axs[0, 0].text(bar.get_x() + bar.get_width() / 2, yval, int(yval), ha="center", va="bottom")
    axs[0, 0].set_xticks(sorted(list(categories_counts1.index)))
    axs[0, 0].set_xlabel(title1, fontsize=18)
    axs[0, 0].set_ylim([0, 900])

    name2 = var_cat_names[1]
    title2 = titles[1]
    categories_counts2 = data[name2].value_counts()
    bars2 = axs[0, 1].bar(
        categories_counts2.index, categories_counts2.values, color="green", edgecolor="black", alpha=0.4
    )
    for bar in bars2:
        yval = bar.get_height()
        axs[0, 1].text(bar.get_x() + bar.get_width() / 2, yval, int(yval), ha="center", va="bottom")
    axs[0, 1].set_xticks(sorted(list(categories_counts2.index)))
    axs[0, 1].set_xlabel(title2, fontsize=18)
    axs[0, 1].set_ylim([0, 600])

    name3 = var_cat_names[2]
    title3 = titles[2]
    categories_counts3 = data[name3].value_counts()
    bars3 = axs[1, 0].bar(
        categories_counts3.index, categories_counts3.values, color="green", edgecolor="black", alpha=0.4
    )
    for bar in bars3:
        yval = bar.get_height()
        axs[1, 0].text(bar.get_x() + bar.get_width() / 2, yval, int(yval), ha="center", va="bottom")
    axs[1, 0].set_xticks(list(categories_counts3.index))
    axs[1, 0].set_xlabel(title3, fontsize=18)
    axs[1, 0].set_ylim([0, 2500])

    name4 = var_cat_names[3]
    title4 = titles[3]
    categories_counts4 = data[name4].value_counts()
    bars4 = axs[1, 1].bar(
        categories_counts4.index, categories_counts4.values, color="green", edgecolor="black", alpha=0.4
    )
    for bar in bars4:
        yval = bar.get_height()
        axs[1, 1].text(bar.get_x() + bar.get_width() / 2, yval, int(yval), ha="center", va="bottom")
    axs[1, 1].set_xticks(sorted(list(categories_counts4.index)))
    axs[1, 1].set_xlabel(title4, fontsize=18)
    axs[1, 1].set_ylim([0, 2500])
    plt.tight_layout()


def plot_numeriques(data, var_num_names, norma=False):
    density = False
    kde = False
    if norma:
        density = True
        kde = True

    fig, axs = plt.subplots(3, 2, figsize=(10, 12))
    # plt.suptitle("Variables numériques")

    titles = [el.replace('_', ' ').title() for el in var_num_names]
    name1 = var_num_names[0]
    title1 = titles[0]
    sns.histplot(data[name1], bins=40, kde=kde, color="green", ax=axs[0, 0], alpha=0.4)
    axs[0, 0].set_xlabel(title1, fontsize=18)
    axs[0, 0].set_ylabel("")

    name2 = var_num_names[1]
    title2 = titles[1]
    sns.histplot(data[name2], bins=40, kde=kde, color="green", ax=axs[0, 1], alpha=0.4)
    axs[0, 1].set_xlabel(title2, fontsize=18)
    axs[0, 1].set_ylabel("")

    name3 = var_num_names[2]
    title3 = titles[2]
    sns.histplot(data[name3], bins=40, kde=kde, color="green", ax=axs[1, 0], alpha=0.4)
    axs[1, 0].set_xlabel(title3, fontsize=18)
    axs[1, 0].set_ylabel("")

    name4 = var_num_names[3]
    title4 = titles[3]
    sns.histplot(data[name4], bins=40, kde=kde, color="green", ax=axs[1, 1], alpha=0.4)
    axs[1, 1].set_xlabel(title4, fontsize=18)
    axs[1, 1].set_ylabel("")

    name5 = var_num_names[4]
    title5 = titles[4]
    sns.histplot(data[name5], bins=40, kde=True, color="green", ax=axs[2, 0], alpha=0.4)
    axs[2, 0].set_xlabel(title5, fontsize=18)
    axs[2, 0].set_ylabel("")

    fig.delaxes(axs[2, 1])

    plt.tight_layout()
    # name5 = var_num_names[4]
    # sns.histplot(data[name5], bins=40, kde=kde, color='green', ax=axs[1, 0])
    # axs[1, 0].set_title(name5)
    # axs[1, 0].set_xlabel('')
    # axs[1, 0].set_ylabel('')

    # name6 = var_num_names[5]
    # sns.histplot(data[name6], bins=40, kde=kde, color='green', ax=axs[1, 1])
    # axs[1, 1].set_title(name6)
    # axs[1, 1].set_xlabel('')
    # axs[1, 1].set_ylabel('')

    # name7 = var_num_names[6]
    # sns.histplot(data[name7], bins=40, kde=kde, color='green', ax=axs[1, 2])
    # axs[1, 2].set_title(name7)
    # axs[1, 2].set_xlabel('')
    # axs[1, 2].set_ylabel('')


def plot_histo_target(target):
    plt.figure(1)
    categories_counts = target.value_counts()
    bars = plt.bar(
        categories_counts.index, categories_counts.values, color="red", edgecolor="black", alpha=0.4
    )
    for bar in bars:
        yval = bar.get_height()
        plt.text(bar.get_x() + bar.get_width() / 2, yval, int(yval), ha="center", va="bottom")
    plt.xticks(sorted(list(categories_counts.index)))
    plt.xlabel("Catégories")
    # plt.title('Variable à prédire : loan_status')
    plt.tight_layout()


def cramers_V(var1, var2):
    crosstab = np.array(pd.crosstab(var1, var2, rownames=None, colnames=None))
    stat = chi2_contingency(crosstab)[0]
    obs = np.sum(crosstab)
    mini = min(crosstab.shape) - 1
    return stat / (obs * mini)


def test_cor_cat(data, vars_categorielles):
    vars_categorielles = pd.DataFrame([data[el] for el in vars_categorielles]).transpose()
    rows = []

    for var1 in vars_categorielles:
        col = []
        for var2 in vars_categorielles:
            cramers = cramers_V(vars_categorielles[var1], vars_categorielles[var2])
            col.append(round(cramers, 2))
        rows.append(col)

    cramers_results = np.array(rows)
    v_cramer_resultats = pd.DataFrame(
        cramers_results, columns=vars_categorielles.columns, index=vars_categorielles.columns
    )
    print("VARIABLES CATEGORIELLES TROP CORRELEES : ")
    count = 0
    for i in range(v_cramer_resultats.shape[0]):
        for j in range(i + 1, v_cramer_resultats.shape[0]):
            if v_cramer_resultats.iloc[i, j] > 0.7:
                count += 1
                print()
                print(
                    v_cramer_resultats.index.to_numpy()[i]
                    + " et "
                    + v_cramer_resultats.columns[j]
                    + " sont trop dépendantes, V-CRAMER = "
                    + str(v_cramer_resultats.iloc[i, j])
                )
    if count == 0:
        print("AUCUNE")


def test_cor_num(data, vars_numeriques, plot=False):
    vars_numeriques = pd.DataFrame([data[el] for el in vars_numeriques]).transpose()
    correlations_num = vars_numeriques.corr(method="pearson")
    nb_variables = correlations_num.shape[0]
    print("VARIABLES NUMERIQUES TROP CORRELEES : ")
    count = 0
    for i in range(nb_variables):
        for j in range(i + 1, nb_variables):
            if abs(correlations_num.iloc[i, j]) > 0.7:
                print()
                print(
                    correlations_num.index.to_numpy()[i]
                    + " et "
                    + correlations_num.columns[j]
                    + " sont trop dépendantes, corr = "
                    + str(round(correlations_num.iloc[i, j], 2))
                )
                count += 1
    if count == 0:
        print("AUCUNE")
    if plot:
        noms_attributs_abreges = [attribut.replace('_', ' ').title()[:] for attribut in correlations_num.columns]
        plt.figure(figsize=(8, 6))
        plt.figure(2)
        sns.heatmap(correlations_num, annot=True, cmap="viridis", fmt=".2f", annot_kws={"size": 12})
        # plt.suptitle("Matrice de Corrélation des variables numériques", fontweight='bold')
        plt.xticks(ticks=0.5 + np.array(range(len(correlations_num.columns))), labels=noms_attributs_abreges)
        plt.yticks(ticks=0.5 + np.array(range(len(correlations_num.columns))), labels=noms_attributs_abreges)
        plt.tight_layout()

if __name__ == "__main__":
    from process_data import load_data, transform_data, types_var, drop

    data, target = load_data()
    var_num_names, var_cat_names, var_bin_names = types_var(data)
    vars_cat = var_cat_names + var_bin_names

    ## DROPPER DES COLONNES
    data, var_num_names = drop(
        data, ["luxury_assets_value", "bank_asset_value"], var_num_names
    )

    test_cor_cat(data, vars_cat)
    print()
    test_cor_num(data, var_num_names, plot=True)

    plot_histo_target(target)
    plot_categorielles(data, var_cat_names + var_bin_names)
    plot_numeriques(data, var_num_names, norma=True)