import numpy as np
import pandas as pd
from sklearn.preprocessing import OneHotEncoder, StandardScaler


def load_data():
    path = "./data/loan_approval_dataset.csv"
    data = pd.read_csv(path)
    data = data.drop("loan_id", axis=1)

    data["loan_status"] = data["loan_status"].replace(" Approved", 1)
    data["loan_status"] = data["loan_status"].replace(" Rejected", 0)

    data["education"] = data["education"].replace(" Graduate", 1)
    data["education"] = data["education"].replace(" Not Graduate", 0)

    data["self_employed"] = data["self_employed"].replace(" Yes", 1)
    data["self_employed"] = data["self_employed"].replace(" No", 0)

    target = data["loan_status"]
    data = data.drop("loan_status", axis=1)
    return data, target


def types_var(data, verbose=False):
    variables_numeriques = []
    variables_categorielles = []
    variables_binaires = []

    for colu in data.columns:
        if str(data[colu].dtypes) in ["int32", "int64", "float64"]:
            if len(data[colu].unique()) == 2:
                variables_binaires.append(colu)
            else:
                if len(data[colu].unique()) < 50:
                    variables_categorielles.append(colu)
                else:
                    variables_numeriques.append(colu)
        else:
            if len(data[colu].unique()) == 2:
                variables_binaires.append(colu)
            else:
                variables_categorielles.append(colu)
    if verbose:
        print(f"Nombre de variables numériques : {len(variables_numeriques)}")
        print(f"Nombre de variables catégorielles : {len(variables_categorielles)}")
        print(f"Nombre de variables binaires : {len(variables_binaires)}")
    return variables_numeriques, variables_categorielles, variables_binaires


def drop(data, names, l):
    for name in names:
        l.remove(name)
        data = data.drop(name, axis=1)
    return data, l


def transform_data(data):
    var_num_names, var_cat_names, var_bin_names = types_var(data)
    data, var_num_names = drop(
        data, ["luxury_assets_value", "bank_asset_value"], var_num_names
    )

    var_cat = pd.DataFrame([data[col] for col in var_cat_names]).transpose()
    var_bin = pd.DataFrame([data[col] for col in var_bin_names]).transpose()
    var_num = pd.DataFrame([data[col] for col in var_num_names]).transpose()

    ### ONE HOT ENCODING FOR CATEGORICAL DATA
    preproc_ohe = OneHotEncoder(drop="first", sparse_output=False).fit(var_cat)
    variables_categorielles_ohe = preproc_ohe.transform(var_cat)
    variables_categorielles_ohe = pd.DataFrame(
        variables_categorielles_ohe, columns=preproc_ohe.get_feature_names_out(var_cat.columns)
    )

    ### NUMERIC DATA NORMALISATION
    scaler = StandardScaler()
    scaler.fit(var_num)
    var_num_scaled = pd.DataFrame(scaler.transform(var_num), columns=var_num.columns)
    # var_num_scaled = var_num

    ### MERGE DATASET
    X_temp = var_num_scaled.merge(variables_categorielles_ohe, left_index=True, right_index=True)
    X = X_temp.merge(var_bin, left_index=True, right_index=True)
    return X


if __name__ == "__main__":
    data, target = load_data()
    var_num_names, var_cat_names, var_bin_names = types_var(data, verbose=True)
