import matplotlib.pyplot as plt  # ; plt.ion()
import numpy as np
import pandas as pd
from sklearn.inspection import permutation_importance
from sklearn.model_selection import train_test_split, cross_val_score, StratifiedKFold, KFold
from sklearn.metrics import (
    accuracy_score,
    f1_score,
    precision_score,
    recall_score,
    multilabel_confusion_matrix,
    roc_curve,
    roc_auc_score,
    precision_recall_curve,
    confusion_matrix,
    cohen_kappa_score,
    auc,
)
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
import seaborn as sns
from process_data import load_data, transform_data
from models import Random, Combine
from sklearn.neural_network import MLPClassifier
import argparse

parser = argparse.ArgumentParser()
parser.add_argument(
    "--mode", type=str, help="Choose the mode : CV, ROC, PR, compare. Set to None by default", default=None
)
parser.add_argument(
    "--model",
    type=str,
    help="If mode is None, choose the model : random, logistic, tree, forest, GradientBoosting, MLP. Set to forest by default",
    default="GradientBoosting",
)
parser.add_argument(
    "--confusion", dest="confusion", default=True, action="store_true", help="Plot confusion matrices"
)
parser.add_argument(
    "--plot_features",
    dest="plot_features",
    default=True,
    action="store_true",
    help="Plot features importances in the prediction",
)
args = parser.parse_args()

mode = args.mode
MODEL = args.model
confusion = args.confusion
plot_features = args.plot_features
opti = True
seed = 42

plt.rcParams.update({"font.size": 12, "text.usetex": True, "text.latex.preamble": r"\usepackage{amsfonts}"})


### LOADING AND PROCESSING DATA
data, target = load_data()
X = transform_data(data)
# l = list(range(X.shape[1]))
# l.remove(2)
# X = X.iloc[:, l]
# X = X.iloc[:, [2]]
y = target

### MODE
mode2name = {
    "CV": "CROSS-VALIDATION",
    "ROC": "COURBES ROC",
    "PR": "COURBES PRECISION-RECALL",
    None: "TEST SIMPLE D'UN MODELE",
    "compare": "COMPARAISON DES MODELES",
    "COMBIN": "COMBINAISON DE MODELES",
}
print()
print("**** MODE : " + mode2name[mode] + " ****")
print()

### POIDS DANS LA COMBINAISON DE MODELES

# w_mlp, w_log, w_gb = 0, 0, 1
# w_mlp, w_rf, w_gb = 1/3, 0, 5/9
# w_log, w_mlp, w_gb = 1/3, 0, 13/18
w_log, w_mlp, w_gb = 0.0, 0.25, 0.5
w_tree = 1 - (w_log + w_mlp + w_gb)

# MODELS
MODELS = ["random", "logistic", "tree", "forest", "GradientBoosting", "MLP", "combin"][:3]
model2name = {
    "random": "Modele aléatoire",
    "logistic": "Régression Logistique",
    "tree": "Arbre de décision",
    "forest": "Random Forest",
    "GradientBoosting": "Gradient Boosting",
    "MLP": "MLP Classifier",
    "combin": "Combinaison de modèles",
}
random_param = {"random_state": seed}
if opti:
    logistic_param = {"random_state": seed, "C": 0.8, "penalty": "l2"}
    tree_param = {
        "random_state": seed,
        "criterion": "log_loss",
        "max_depth": 50,
        "min_samples_leaf": 4,
        "min_samples_split": 10,
    }
    forest_param = {
        "random_state": seed,
        "bootstrap": False,
        "max_depth": None,
        "min_samples_leaf": 4,
        "min_samples_split": 20,
        "n_estimators": 40,
    }
    GB_param = {
        "random_state": seed,
        "learning_rate": 0.2,
        "max_depth": 4,
        "min_samples_leaf": 5,
        "min_samples_split": 5,
        "n_estimators": 300,
    }
    MLP_param = {
        "random_state": seed,
        "activation": "tanh",
        "alpha": 0.1,
        "hidden_layer_sizes": (100,),
        "learning_rate": "adaptive",
        "max_iter": 1000,
    }

    name2model = {
        "random": Random(**random_param),
        "logistic": LogisticRegression(**logistic_param),
        "tree": DecisionTreeClassifier(**tree_param),
        "forest": RandomForestClassifier(**forest_param),
        "GradientBoosting": GradientBoostingClassifier(**GB_param),
        "MLP": MLPClassifier(**MLP_param),
    }

    combine = Combine(
        {
            w_tree: DecisionTreeClassifier(**tree_param),
            w_log: LogisticRegression(**logistic_param),
            w_mlp: MLPClassifier(**MLP_param),
            w_gb: GradientBoostingClassifier(**GB_param),
        }
    )
    name2model["combin"] = combine

else:
    name2model = {
        "random": Random(**random_param),
        "logistic": LogisticRegression(**random_param),
        "tree": DecisionTreeClassifier(**random_param),
        "forest": RandomForestClassifier(**random_param),
        "GradientBoosting": GradientBoostingClassifier(**random_param),
        "MLP": MLPClassifier(random_state=seed, max_iter=1000),
    }

    combine = Combine(
        {
            w_tree: DecisionTreeClassifier(**random_param),
            w_log: LogisticRegression(**random_param),
            w_mlp: MLPClassifier(**random_param),
            w_gb: GradientBoostingClassifier(**random_param),
        }
    )
    name2model["combin"] = combine

if mode is None:
    print()
    print("NOM DU MODELE : " + model2name[MODEL])
    print()


### TRAIN TEST SPLIT
size = 0.3
print()
print(f"TRAIN-TEST SPLIT : {(1 - size) * 100}% - {size * 100}%")
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=size, random_state=seed)


# CHOOSE THE METRIC
metrics = ["accuracy", "f1", "precision", "recall", "kappa"]
ref_score = "kappa"
lab2names = {
    "accuracy": "Accuracy",
    "f1": "F1-Score",
    "precision": "Precision",
    "recall": "Recall",
    "kappa": "Kappa Score",
}
plot_feat_importance = True


### DEFINE THE MODEL
model = name2model[MODEL]


if mode == "COMBIN":
    tree = name2model["tree"]
    gb = name2model["GradientBoosting"]
    rf = name2model["forest"]
    mlp = name2model["MLP"]
    log = name2model["logistic"]

    models = {w_log: log, w_mlp: mlp, w_gb: gb, w_tree: tree}
    model = Combine(models)

    n_splits = 5
    kf = KFold(n_splits=5, shuffle=True, random_state=42)
    scores = []

    # Boucle sur les plis de la validation croisée
    for train_index, test_index in kf.split(X_train, y_train):
        X_t, X_val = np.array(X_train)[train_index], np.array(X_train)[test_index]
        y_t, y_val = np.array(y_train)[train_index], np.array(y_train)[test_index]

        model.fit(X_t, y_t)
        y_pred = model.predict(X_val)

        # Calcul du score
        if ref_score == "kappa":
            score = cohen_kappa_score(y_val, y_pred)
        elif ref_score == "recall":
            score = recall_score(y_val, y_pred)
        elif ref_score == "precision":
            score = precision_score(y_val, y_pred)
        elif ref_score == "accuracy":
            score = accuracy_score(y_val, y_pred)
        elif ref_score == "f1":
            score = f1_score(y_val, y_pred)
        scores.append(score)
    scores = np.array(scores)

    print()
    print("Modele combiné")
    print()
    print("Score utilisé : " + lab2names[ref_score])
    print("Scores des validations croisées : ", scores.round(3))
    print("Moyenne des scores : {:.4f}".format(scores.mean()))
    print("Écart-type des scores : {:.4f}".format(scores.std()))
    print()

    ### PREDICT THE TEST

    model.fit(X_train, y_train)
    y_pred = model.predict(X_test)

    print()
    # PRINT SCORE
    if "accuracy" in metrics:
        accuracy = accuracy_score(y_test, y_pred)
        print("Accuracy sur le test : {:.3f}%".format(accuracy * 100))

    if "precision" in metrics:
        precision = precision_score(y_test, y_pred)
        print("Précision sur le test : {:.3f}".format(precision))

    if "recall" in metrics:
        precision = recall_score(y_test, y_pred)
        print("Recall sur le test : {:.3f}".format(precision))

    if "f1" in metrics:
        f1 = f1_score(y_test, y_pred)
        print("F1-Score sur le test : {:.3f}".format(f1))

    if "kappa" in metrics:
        kappa = cohen_kappa_score(y_test, y_pred)
        print("Kappa score sur le test : {:.3f}".format(kappa))
    print()

    if confusion:
        m = confusion_matrix(y_test, y_pred)
        plt.figure(42)
        # plt.figure(figFalsesize=(8, 6))
        sns.heatmap(
            m,
            annot=False,
            fmt="d",
            cmap="Blues",
            xticklabels=["Prédit 0", "Prédit 1"],
            yticklabels=["Vrai 0", "Vrai 1"],
        )
        plt.xlabel("Prédictions")
        plt.ylabel("Vraies étiquettes")
        plt.title("Matrice de Confusion sur le test")
        plt.text(0.42, 0.52, str(m[0, 0]), color="r", size=15)
        plt.text(1.45, 0.52, str(m[0, 1]), color="r", size=15)
        plt.text(0.45, 1.6, str(m[1, 0]), color="r", size=15)
        plt.text(1.42, 1.6, str(m[1, 1]), color="r", size=15)


elif mode == "CV":
    for MODEL in MODELS:
        model = name2model[MODEL]
        n_splits = 5
        kf = KFold(n_splits=5, shuffle=True, random_state=42)
        scores = []

        # Boucle sur les plis de la validation croisée
        for train_index, test_index in kf.split(X_train, y_train):
            X_t, X_val = np.array(X_train)[train_index], np.array(X_train)[test_index]
            y_t, y_val = np.array(y_train)[train_index], np.array(y_train)[test_index]

            model.fit(X_t, y_t)
            y_pred = model.predict(X_val)

            # Calcul du score
            if ref_score == "kappa":
                score = cohen_kappa_score(y_val, y_pred)
            elif ref_score == "recall":
                score = recall_score(y_val, y_pred)
            elif ref_score == "precision":
                score = precision_score(y_val, y_pred)
            elif ref_score == "accuracy":
                score = accuracy_score(y_val, y_pred)
            elif ref_score == "f1":
                score = f1_score(y_val, y_pred)

            scores.append(score)
        scores = np.array(scores)
        print()
        print("Modele : " + model2name[MODEL])
        print()
        print("Score utilisé : " + lab2names[ref_score])
        print("Scores des validations croisées : ", scores.round(3))
        print("Moyenne des scores : {:.4f}".format(scores.mean()))
        print("Écart-type des scores : {:.4f}".format(scores.std()))
        print()


elif mode == "ROC":
    plt.figure(figsize=(10, 8))
    plt.plot([0, 1], [0, 1], color="gray", linestyle="--", lw=1, label="No skills")
    for name in MODELS:
        model = name2model[name]
        model.fit(X_train, y_train)
        y_scores = model.predict_proba(X_test)[:, 1]
        fpr, tpr, seuils = roc_curve(y_test, y_scores)

        AUC = roc_auc_score(y_test, y_scores)

        # Tracer la courbe ROC
        plt.plot(fpr, tpr, lw=2, label=model2name[name] + " (AUC = {:.4f})".format(AUC))
        plt.xlabel("Taux de Faux Positifs ($\\frac{FP}{FP + TN}$)")
        plt.ylabel("Taux de Vrais Positifs ($\\frac{TP}{TP + FN}$)")
        # plt.title("Courbes ROC")
        plt.legend()


elif mode == "PR":
    plt.figure(figsize=(10, 8))
    p = sum(y_train) / len(y_train)
    plt.plot(
        [0, 1], [p, p], color="gray", linestyle="--", lw=1, label="No skills ($\\frac{TP}{TP + FP + TN + FN}$)"
    )
    for name in MODELS:
        model = name2model[name]
        model.fit(X_train, y_train)
        y_scores = model.predict_proba(X_test)[:, 1]
        precision, recall, seuils = precision_recall_curve(y_test, y_scores)
        AUC = auc(recall, precision)
        # Tracer la courbe P-R
        plt.plot(recall, precision, lw=2, label=model2name[name] + " (AUC = {:.4f})".format(AUC))
        plt.xlabel("Recall " + "= $\\frac{TP}{TP + FN}$")
        plt.ylabel("Precision " + "= $\\frac{TP}{TP + FP}$")
        plt.xlim([0, 1])
        plt.ylim([0.57, 1])
        # plt.title("Courbes Precision-Recall")
        plt.legend()


elif mode == "compare":
    print()
    print("Score utilisé : " + lab2names[ref_score])
    SCORES = {}
    for MODEL in MODELS:
        model = name2model[MODEL]
        model.fit(X_train, y_train)
        y_pred = model.predict(X_test)
        if ref_score == "f1":
            score = f1_score(y_test, y_pred)
        elif ref_score == "recall":
            score = recall_score(y_test, y_pred)
        elif ref_score == "precision":
            score = precision_score(y_test, y_pred)
        elif ref_score == "accuracy":
            score = accuracy_score(y_test, y_pred)
        elif ref_score == "kappa":
            score = cohen_kappa_score(y_test, y_pred)
        SCORES[MODEL] = score

    dico_trie = list(sorted(SCORES.items(), key=lambda item: item[1]))
    dico_trie.reverse()
    print()
    for el in dico_trie:
        name, score = el
        print(model2name[name] + " : " + str(round(score, 3)))


else:
    model.fit(X_train, y_train)
    y_pred = model.predict(X_train)

    print()
    # PRINT SCORE
    if "accuracy" in metrics:
        accuracy = accuracy_score(y_train, y_pred)
        print("Accuracy sur le train : {:.3f}%".format(accuracy * 100))

    if "precision" in metrics:
        precision = precision_score(y_train, y_pred)
        print("Précision sur le train : {:.3f}".format(precision))

    if "recall" in metrics:
        precision = recall_score(y_train, y_pred)
        print("Recall sur le train : {:.3f}".format(precision))

    if "f1" in metrics:
        f1 = f1_score(y_train, y_pred)
        print("F1-Score sur le train : {:.3f}".format(f1))

    if "kappa" in metrics:
        kappa = cohen_kappa_score(y_train, y_pred)
        print("Kappa score sur le train : {:.3f}".format(kappa))

    if confusion:
        m = multilabel_confusion_matrix(y_train, y_pred)[1]
        plt.figure(figsize=(6, 6))
        sns.heatmap(
            m,
            annot=False,
            fmt="d",
            cmap="Blues",
            xticklabels=["Prédit 0", "Prédit 1"],
            yticklabels=["Vrai 0", "Vrai 1"],
        )
        plt.xlabel("Prédictions")
        plt.ylabel("Vraies étiquettes")
        plt.title("Matrice de Confusion sur le train")
        plt.text(0.4, 0.52, str(m[0, 0]), color="r", size=15)
        plt.text(1.45, 0.52, str(m[0, 1]), color="r", size=15)
        plt.text(0.45, 1.6, str(m[1, 0]), color="r", size=15)
        plt.text(1.4, 1.6, str(m[1, 1]), color="r", size=15)

    ### PREDICT THE TEST
    y_pred = model.predict(X_test)

    print()
    # PRINT SCORE
    if "accuracy" in metrics:
        accuracy = accuracy_score(y_test, y_pred)
        print("Accuracy sur le test : {:.3f}%".format(accuracy * 100))

    if "precision" in metrics:
        precision = precision_score(y_test, y_pred)
        print("Précision sur le test : {:.3f}".format(precision))

    if "recall" in metrics:
        precision = recall_score(y_test, y_pred)
        print("Recall sur le test : {:.3f}".format(precision))

    if "f1" in metrics:
        f1 = f1_score(y_test, y_pred)
        print("F1-Score sur le test : {:.3f}".format(f1))

    if "kappa" in metrics:
        kappa = cohen_kappa_score(y_test, y_pred)
        print("Kappa score sur le test : {:.3f}".format(kappa))
    print()

    if confusion:
        m = confusion_matrix(y_test, y_pred)
        plt.figure(42)
        # plt.figure(figFalsesize=(8, 6))
        sns.heatmap(
            m,
            annot=False,
            fmt="d",
            cmap="Blues",
            xticklabels=["Prédit 0", "Prédit 1"],
            yticklabels=["Vrai 0", "Vrai 1"],
        )
        plt.xlabel("Prédictions")
        plt.ylabel("Vraies étiquettes")
        plt.title("Matrice de Confusion sur le test")
        plt.text(0.42, 0.52, str(m[0, 0]), color="r", size=15)
        plt.text(1.45, 0.52, str(m[0, 1]), color="r", size=15)
        plt.text(0.45, 1.6, str(m[1, 0]), color="r", size=15)
        plt.text(1.42, 1.6, str(m[1, 1]), color="r", size=15)

    if plot_feat_importance:
        if MODEL in ["forest", "tree", "random", "GradientBoosting", "logistic", "MLP"]:
            perm_importance = permutation_importance(model, X_test, y_test)
            feature_names = list(X_test.columns)
            features = np.array(feature_names)

            sorted_idx = perm_importance.importances_mean.argsort()
            plt.figure(43)
            plt.barh(features[sorted_idx], perm_importance.importances_mean[sorted_idx])
            plt.xlabel("Features Importance in prediction")
            plt.suptitle(model2name[MODEL])


if mode in ["CV", "ROC", "PR"] or confusion or plot_feat_importance:
    plt.show()
